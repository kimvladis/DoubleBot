import 'jquery-ui';
import 'bootstrap';
import "../../node_modules/bootstrap/dist/css/bootstrap.css";
import './components/bootstrap-fileinput';
import './css/main.css';

var defaultHost        = 'http://185.143.173.201:3000';
Array.prototype.remove = function () {
  var what, a = arguments, L = a.length, ax;
  while ( L && this.length ) {
    what = a[ --L ];
    while ( (ax = this.indexOf(what)) !== -1 ) {
      this.splice(ax, 1);
    }
  }
  return this;
};

var $items  = $('#items');
var $errors = $('#errors');

// Saves options to chrome.storage.sync.
function save_options (event) {
  event.preventDefault();
  var
    maxCount    = document.getElementById('max_count').value,
    host        = $('#host').val(),
    autoload    = $('#autoload').prop('checked'),
    autoconfirm = $('#autoconfirm').prop('checked');
  chrome.storage.local.set(
    {
      maxCount:    maxCount,
      host:        host,
      autoload:    autoload,
      autoconfirm: autoconfirm
    }, function () {
      // Update status to let user know options were saved.
      if ( host != defaultHost ) {
        chrome.permissions.request(
          {
            origins: [ host ]
          }, function (granted) {
            if ( !granted ) {
              console.log('Host rollback');
            } else {
              console.log('New host added');

            }
          }
        );
      }
      var status         = document.getElementById('status');
      status.textContent = 'Options saved.';
      setTimeout(function () {
                   status.textContent = '';
                 }, 750
      );
    }
  );

  return false;
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options () {
  // Use default value color = 'red' and likesColor = true.
  chrome.storage.local.get(
    {
      items:       [],
      allItems:    [],
      errors:      [
        'Items no longer available - please reload the page and try again.',
        'Please confirm your existing offer before making another.',
        'Please select at least 1 item.'
      ],
      maxCount:    2,
      host:        defaultHost,
      autoload:    false,
      autoconfirm: false
    }, function (result) {
      defaultHost = result.host;
      repaintItems(result.allItems, result.items);
      repaintErrors(result.errors);
      chrome.storage.local.set({ errors: result.errors });
      $('#max_count').val(result.maxCount);
      $('#host').val(result.host);
      $('#autoload').prop('checked', result.autoload);
      $('#autoconfirm').prop('checked', result.autoconfirm);
    }
  );
}
function getItem (item, isOn) {
  return "<li class='list-group-item row'>" +
    "<input type='checkbox' class='item-checkbox' " + (isOn ? 'checked' : '') + ">" +
    "<span class='item-name'>" + item + "</span>" +
    "<button data-val='" + item + "' class='remove_item btn btn-default pull-right'>-</button></li>";
}
function getError (item) {
  return "<li class='list-group-item row'>" +
    "<span class='item-name'>" + item + "</span>" +
    "<button data-val='" + item + "' class='remove_error btn btn-default pull-right'>-</button></li>";
}
function removeItemHandler () {
  const item = $(this).data('val');
  chrome.storage.local.get(
    { allItems: [], items: [] }, function (result) {
      var allItems = result.allItems;
      var items    = result.items;
      allItems.remove(item + '');
      items.remove(item + '');
      chrome.storage.local.set({ items: items, allItems: allItems });
    }
  );
}
function addItemHandler () {
  const item = $('#item').val();
  chrome.storage.local.get(
    { items: [], allItems: [] }, function (result) {
      var allItems = result.allItems;
      var items    = result.items;
      items.remove(item);
      items.push(item);
      allItems.remove(item);
      allItems.push(item);
      chrome.storage.local.set({ items: items, allItems: allItems });
    }
  );
}
function clearHandler () {
  chrome.storage.local.set({ items: [], allItems: [] });
}
function removeErrorHandler () {
  const item = $(this).data('val');
  chrome.storage.local.get(
    { errors: [] }, function (result) {
      var items = result.errors;
      items.remove(item);
      chrome.storage.local.set({ errors: items });
    }
  );
}
function addErrorHandler () {
  const item = $('#error').val();
  chrome.storage.local.get(
    { errors: [] }, function (result) {
      var errors = result.errors;
      errors.remove(item);
      errors.push(item);
      chrome.storage.local.set({ errors: errors });
    }
  );
}
function repaintItems (allItems, items) {
  $items.html(
    allItems.reduce(
      function (prev, curr) {
        var isOn = $.inArray(curr, items) >= 0;
        return prev += getItem(curr, isOn);
      }, ''
    )
  )
}
function repaintErrors (items) {
  $errors.html(
    items.reduce(
      function (prev, curr) {
        return prev += getError(curr);
      }, ''
    )
  )
}
chrome.storage.onChanged.addListener(
  function (changes, namespace) {
    for ( let key in changes ) {
      var storageChange = changes[ key ];
      if ( key == 'allItems' ) {
        chrome.storage.local.get({ items: [], allItems: [] }, function (result) {
                                   repaintItems(result.allItems, result.items);
                                 }
        );
      } else if ( key == 'errors' ) {
        repaintErrors(storageChange.newValue);
      }
    }
  }
);

$(document).on('click', '.remove_item', removeItemHandler);
$(document).on('click', '.remove_error', removeErrorHandler);
$(document).on('click', '#add_item', addItemHandler);
$(document).on('click', '#clear', clearHandler);
$(document).on('click', '#add_error', addErrorHandler);
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',
                                                 save_options
);

$(function () {
    $(document).on(
      'change', '.item-checkbox', function () {
        var allItems = $('#items').find('.item-name').map(function (i, curr, arr) {
                                                            return $(curr).text();
                                                          }
        ).get();
        var items    = $('#items').find('input:checked').map(function (i, curr, arr) {
                                                               return $(curr).next().text();
                                                             }
        ).get();

        chrome.storage.local.set({ items: items, allItems: allItems });
      }
    );
    $("#items").sortable();
    $("#items").disableSelection();
    $("#items").on(
      'sortupdate', function () {
        var allItems = $('#items').find('.item-name').map(
          function (i, curr, arr) {
            return $(curr).text();
          }
        ).get();
        var items    = $('#items').find('input:checked').map(
          function (i, curr, arr) {
            return $(curr).next().text();
          }
        ).get();

        chrome.storage.local.set({ items: items, allItems: allItems });
      }
    );
    var $fileChooser = $('input[type=file]');
    $fileChooser.bootstrapFileInput();
    $fileChooser.on(
      'change', function (evt) {
        var f = evt.target.files[ 0 ];
        if ( f ) {
          var reader    = new FileReader();
          reader.onload = function (e) {
            var contents    = e.target.result;
            var importItems = contents.split("\n");
            if ( importItems[ 0 ] != "CSGODOUBLE_BOT_IMPORT" ) {
              alert('Unsupported file type');
              return false;
            } else {
              importItems.shift();
            }
            chrome.storage.local.get(
              { items: [], allItems: [] }, function (result) {
                var allItems = result.allItems;
                var items    = result.items;

                importItems.forEach(
                  function (item, i, arr) {
                    if ( item != "" ) {
                      items.remove(item);
                      items.push(item);
                      allItems.remove(item);
                      allItems.push(item);
                    }
                  }
                );

                chrome.storage.local.set({ items: items, allItems: allItems });
              }
            );
          };
          reader.readAsText(f);
        }
      }
    );
  }
);