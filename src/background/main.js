import App from './components/App';

let app = new App();

$(function() {
  let interval = setInterval(
    function () {
      app.isItemsLoaded = $('#left_number').text() != 0;
      if ( app.isItemsLoaded ) {
        clearInterval(interval);
        app.afterLoad();
      }
    }, 10
  );
});