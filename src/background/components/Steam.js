import App from './App';
const $you_notready = $("#you_notready");

export default class Steam {
  constructor () {
    this.app = new App();
  }

  setSteamLink(link) {
    this.app.steamLinkOpened = true;
    this.app.steamLink       = link;
    setTimeout(this.checkTab.bind(this), 5000);
  }

  checkTab () {
    chrome.tabs.query({ url: this.app.steamLink }, function (tabs) {
      if (tabs.length > 0) {
        this.app.steamLinkOpened = true;
        setTimeout(this.checkTab.bind(this), 1000);
      } else {
        this.app.sendMessage({action: 'checkTab', steamLinkOpened: false});
      }
    }.bind(this))
  }
}