import Steam from './Steam';


let instance = null;

export default class App {
  constructor () {
    if ( !instance ) {
      instance = this;

      this.steamLinkOpened = false;
      this.steamLink       = "";
      this.tabId           = null;
      /**
       * @type {Steam}
       */
      this.steam  = new Steam();

      chrome.runtime.onMessage.addListener(this.onMessage.bind(this));
    }

    return instance;
  }

  onMessage (request, sender, sendResponse) {
    this.tabId = sender.tab.id;

    if ( request.action == 'checkTabs' ) {
      this.steam.setSteamLink(request.url);
    }

    if ( request.action == 'isOpened' ) {
      sendResponse({steamLinkOpened: this.steamLinkOpened});
      return;
    }

    sendResponse({success: true});
  }

  sendMessage (request, callback) {
    if (this.tabId) {
      chrome.tabs.sendMessage(this.tabId, request, function(response) {
        if (callback) callback(response);
      });
    }
  }
}