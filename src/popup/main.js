import 'bootstrap';
import "../../node_modules/bootstrap/dist/css/bootstrap.css";
import "./css/main.css";

/**
 * Get the current URL.
 *
 * @param {function(string)} callback - called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
    var tab = tabs[0];
    var url = tab.url;
    console.assert(typeof url == 'string', 'tab.url should be a string');

    callback(url, tab.id);
  });
}

function getCurrentTabId(callback) {
  var query = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(query, function(tabs) {
    var tab = tabs[0];
    var id = tab.id;
    console.log(tab);
    console.log(tab.id + ' - ' + tab.url);

    callback(id);
  })
}

var tabId;

function getCsUrl() {
  return "https://csgopolygon.com/withdraw.php";
}

function isCsUrl(url) {
  // Return whether the URL starts with the Gmail prefix.
  return url.indexOf(getCsUrl()) == 0;
}

function sendMessage(action, callback) {
  action = action || 'start';
  if (tabId) {
    chrome.tabs.sendMessage(tabId, {action:action}, function(response) {
      console.log(action + ' action sent');
      if (callback) callback(response);
    });
  } else {
    getCurrentTabUrl(function(url, id) {
      if (isCsUrl(url)) {
        tabId = id;
        sendMessage(action, callback);
      } else {
        alert('You are not on csdouble page.')
      }
    });
  }
}

$(document).on('click', '#start', function() {
  sendMessage('start');
});

$(document).on('click', '#stop', function() {
  sendMessage('stop');
});
