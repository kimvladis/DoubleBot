import App from './App';

export default class Picker {
  constructor () {
    this.app = new App();
  }

  checkHeld (id) {
    if ( this.app.heldItems.indexOf(id) >= 0 ) {
      return true;
    }

    let held = $.ajax(
      {
        type:  "GET",
        url:   this.app.urlTo(['bot','check',id]),
        async: false
      }
    ).responseJSON.held;

    if ( held ) {
      this.app.heldItems.push(id);
      return true
    }

    return false;
  }

  pickItem ($item) {
    var price = $item.data('price'),
        id    = $item.data('id');

    var check = this.checkHeld(id);

    if ( !check && price < this.app.balance ) {
      $item.trigger('click');
      this.app.balance -= price;
      this.app.socket.emit('hold', { id: id });
      return true;
    }

    if ( check ) {
      console.log('Item already held');
    } else if ( price < this.app.balance ) {
      console.log('Not enough money');
    }

    return false;
  }

  pickItems (items) {
    var botId;
    var count = 0;
    for ( let key in items ) {
      if (!items.hasOwnProperty(key)) continue;

      var item   = items[ key ];
      var $items = $('#left').find('div.placeholder:not(.hidden) div[data-name="' + item + '"]');
      for ( var i = 0; i < $items.length; i++ ) {
        var $item = $($items[ i ]);
        if ( botId === undefined ) {
          if ( this.pickItem($item) ) {
            botId = $item.data('bot');
            count++;
          }
        } else if ( botId === $item.data('bot') ) {
          if ( this.pickItem($item) ) {
            count++;
          }
        }
        if ( count >= this.app.maxCount ) {
          return;
        }
      }
    }
  }
}