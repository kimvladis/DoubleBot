import Double from "./Double";
import Status from "./Status";
import Socket from "./Socket";
import Picker from "./Picker";
import Steam from './Steam';
import View from './View';

let instance = null;

export default class App {
  constructor () {
    if ( !instance ) {
      instance = this;

      this.time            = new Date();
      this.stop            = false;
      this.isOnGoing       = false;
      this.necessaryItems  = [];
      this.maxCount        = 2;
      this.stopErrors      = [];
      this.isItemsLoaded   = false;
      this.balance         = 0;
      this.steamLinkOpened = false;
      this.steamLink       = "";
      this.host            = null;
      this.heldItems       = [];
      this.isReady         = false;
      this.autoload        = false;
      this.autoconfirm     = false;

      /**
       * @type {Double}
       */
      this.double = new Double();
      /**
       * @type {Socket}
       */
      this.socket = new Socket();
      /**
       * @type {Status}
       */
      this.status = new Status();
      /**
       * @type {Picker}
       */
      this.picker = new Picker();
      /**
       * @type {Steam}
       */
      this.steam  = new Steam();

      chrome.storage.local.get(
        {
          items:       [],
          errors:      [],
          maxCount:    2,
          autoload:    false,
          autoconfirm: false,
          tid:         false,
          data:        [],
        }, function (result) {
          this.maxCount       = result.maxCount;
          this.necessaryItems = result.items;
          this.stopErrors     = result.errors;
          this.autoload       = result.autoload;
          this.autoconfirm    = result.autoconfirm;
          if ( result.tid ) {
            View.setTid(result.tid);
            this.double.confirming(result.data);
          }

        }.bind(this)
      );

      chrome.runtime.onMessage.addListener(this.onMessage.bind(this));
    }

    return instance;
  }

  /**
   * Create url
   *
   * @param {[]} params
   * @returns {string}
   */
  urlTo (params = []) {
    let url = /\/$/.test(this.host) ? this.host : this.host + '/';
    return url + params.join('/');
  }

  startExtension () {
    console.log('Starting Extension');

    this.setSteamLinkOpend(false);
    if ( this.isOnGoing ) {
      console.log('already started');
    } else {
      this.stop      = false;
      this.isOnGoing = true;
      this.status.notify(false);

      chrome.storage.local.get(
        { items: [], errors: [], maxCount: 2 }, function (result) {
          this.maxCount       = result.maxCount;
          this.necessaryItems = result.items;
          this.stopErrors     = result.errors;

          if ( Double.isCartEmpty() ) {
            this.picker.pickItems(this.necessaryItems);
          }
          this.double.offering();
        }.bind(this)
      );
    }
  }

  refresh () {
    this.setSteamLinkOpend(false);
    console.log('Refreshing Extension');
  }

  stopExtension () {
    this.setSteamLinkOpend(false);
    this.stop            = true;
    this.isOnGoing       = false;
    this.status.notify(true);
    console.log('Stopping Extension');
  }

  afterLoad () {
    console.log('loaded');
    this.balance = parseInt($("#avail").text().replace(/\,/g, ""));
    if ( this.autoload ) {
      this.startExtension();
    }
  }

  onMessage (request, sender, sendResponse) {
    if ( request.action == 'start' ) {
      this.startExtension();
    } else if ( request.action == 'stop' ) {
      this.stopExtension();
    } else if ( request.action == 'reconnect' ) {
      this.setUpSocket();
    } else if ( request.action == 'checkTab' ) {
      this.setSteamLinkOpend(request.steamLinkOpened);
    }

    sendResponse({});
  }

  setSteamLinkOpend(data) {
    this.steamLinkOpened = data;
  }
}