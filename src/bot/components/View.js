const $inlineAlert = $('#inlineAlert');
const $confirmButton = $("#confirmButton");

export default class View {
  static inlineAlert (x, y) {
    $inlineAlert.removeClass("alert-success alert-danger alert-warning hidden");
    if ( x == "success" ) {
      $inlineAlert.addClass("alert-success").html("<i class='fa fa-check'></i><b> " + y + "</b>");
    } else if ( x == "error" ) {
      $inlineAlert.addClass("alert-danger").html("<i class='fa fa-exclamation-triangle'></i> " + y);
    } else if ( x == "cross" ) {
      $inlineAlert.addClass("alert-danger").html("<i class='fa fa-times'></i> " + y);
    } else {
      $inlineAlert.addClass("alert-warning").html("<b>" + y + " <i class='fa fa-spinner fa-spin'></i></b>");
    }
  }

  /**
   *
   * @param {{ bot:Number, code:Number, amount:Number, state:Number }} data
   */
  static showPending (data) {
    var content = "<b>New trade from " + data.bot + " ";
    content += "with security code " + data.code + " for " + data.amount + " credits.";
    content += " Please <a href='https://steamcommunity.com/tradeoffer/" + data.acceptid +
      "' target='_blank' >accept the trade offer</a> then click confirm";
    if ( data.amount > 0 ) {
      content += " to collect credits.";
    } else {
      content += ".";
    }
    $("#offerContent").html(content);
    $confirmButton.data("tid", data.tid);
    if ( data.amount < 0 ) {
      if ( data.state == 2 || data.state == 3 || data.state == 4 || data.state == 9 ) {
        $confirmButton.html("Check Status");
      } else {
        $confirmButton.html("Refund Coins");
      }
    }
    $("#offerPanel").slideDown();
  }

  /**
   *
   * @param { Number } tid
   */
  static setTid(tid) {
    $confirmButton.data("tid", tid);
  }
}