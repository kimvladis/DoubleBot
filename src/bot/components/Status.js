import App from './App';

export default class Status {
  constructor () {
    this.app = new App();
    this.check();
  }

  /**
   * Tell server about bot's status
   *
   * @param isReady
   */
  notify (isReady) {
    if (isReady === undefined) {
      isReady = this.app.isReady;
    }
    this.app.isReady = isReady;
    this.app.socket.emit('ready', isReady);
  }

  /**
   * Checks session expire
   */
  check () {
    let _update = () => this.check();

    if (!this.app.isOnGoing) {
      $.get(
        '/scripts/_withdraw.php?assetids=1&tradeurl=&checksum=1',
        function (data) {
          data = JSON.parse(data);
          console.log(data);

          if ( !data.success ) {
            var _isReady = data.error.indexOf('Withdraw session expired') < 0;

            if ( this.app.isReady != _isReady ) {
              this.notify(_isReady);

              let message = this.app.isReady ? 'ready' : 'expired';
              console.log(message);

              if ( !this.app.isReady ) {
                alert(message);
              }

              setTimeout(_update, 1000 * 60 * 10);

              return;
            }
          }

          setTimeout(_update, 1000);
        }.bind(this)
      );
    } else {
      setTimeout(_update, 1000);
    }
  }
}