import App from './App';
const $you_notready = $("#you_notready");
var steamInterval;

export default class Steam {
  constructor () {
    this.app = new App();
  }

  acceptSteamTrade () {
    var text = $('#error_msg').text();
    var foundin = $('*:contains("Подтвердить")');
    console.log(foundin);
    if (text) {
      if (text.indexOf('предложение обмена больше не действительно') >= 0) {
        window.close();
      } else {
        setTimeout(function () {
          location.reload();
        }, 5000);
      }
    }

    if ( $("div").is("#you_notready") ) {
      if ( !$("#their_slot_0").hasClass("has_item") || $("#your_slot_0").hasClass("has_item") ) {
        console.log("Не выводит");
        setTimeout(function () {
          location.reload();
        }, 5000);

        return;
      }

      steamInterval = setInterval(
        function () {
          if (!this.app.autoconfirm) {
            clearInterval(steamInterval);
            return;
          }
          if ( $you_notready.attr("style") != "display: none;" ) {
            $you_notready.click();
            $(".newmodal_buttons > .btn_green_white_innerfade").click();
          }
          else {
            $("#trade_confirmbtn").click();
            setTimeout(function () {
              location.reload();
            }, 5000);
          }
        }.bind(this), 2000
      );
    }
    else if ( $("div").is(".received_items_header") ) {
      if ( $(".received_items_header").html().indexOf("Обмен завершен") != -1 ) {
        window.close();
      }
    }
  }

  setSteamLink(link) {
    this.app.setSteamLinkOpend(true);
    this.app.steamLink       = link;
    this.checkTab();
  }

  isOpened () {
    return this.app.steamLinkOpened;
  }

  checkTab () {
    chrome.runtime.sendMessage({ action: "checkTabs", url: this.app.steamLink}, function(response) {
      console.log(response);
    }.bind(this));
  }
}