import App from "./App";
import View from "./View";
import "../tools/findPartial";

const $right = $('#right');

export default class Double {
  constructor () {
    this.app = new App();
  }

  static isCartEmpty () {
    return $right.find('.reals div').length === 0;
  }

  checkError (error) {
    if (this.app.stopErrors instanceof Array) {
      return this.app.stopErrors.findPartial(error);
    }

    return false;
  }

  /**
   * Take an offer
   *
   * @param {Function} callback
   * @param {{ items:string, sum:Number }} data optional parameter
   */
  offer (callback, data) {
    View.inlineAlert("", "Processing trade offer - please wait...");
    var csv = "";
    var sum = 0;
    var botid = 0;
    if ( data ) {
      csv = data.items;
      sum = data.sum;
      botid = data.botid;
    } else {
      $right.find(".slot").each(
        function () {
          csv += $(this).data("id") + ",";
          sum += $(this).data("price");
          botid = $(this).data('bot');
        }
      );
    }

    var turl     = $("#tradeurl").val();
    var remember = $("#remember").is(":checked") ? "on" : "off";
    var url      = "scripts/_withdraw.php";
    $.ajax(
      {
        "url":   url,
        type:    "GET",
        data:    {
          "assetids": csv,
          "tradeurl": turl,
          "checksum": sum,
          "remember": remember,
          "botid":    botid
        },
        success: function (response) {
          try {
            response = JSON.parse(response);
            if ( response.success ) {
              View.inlineAlert("success", "New trade offer!");
              View.showPending(response);
              chrome.storage.local.set({ tid: response.tid, data: data});
            } else {
              View.inlineAlert("error", response.error);
            }
          } catch ( err ) {
            View.inlineAlert("error", "Javascript error: " + err);
          }
          if ( callback !== undefined ) {
            callback(response, data);
          }
        },
        error:   function (err) {
          View.inlineAlert("error", "AJAX error: " + err.statusText);
          if ( callback !== undefined ) {
            callback(
              {
                success: false,
                error:   "AJAX error: " + err.statusText,
                count:   0
              }, data
            );
          }
        }
      }
    );
  }

  /**
   * Confirm an offer
   *
   * @param {Function} callback
   * @param {{ items:string, sum:Number }} data
   * @returns {boolean}
   */
  confirm (callback, data) {
    View.inlineAlert("", "Confirming trade offer - please wait...");
    let $this = $('#confirmButton');
    $this.prop("disabled", true);
    var tid = $this.data("tid");
    $.ajax(
      {
        url:      "scripts/_confirm.php",
        type:     "GET",
        data:     {
          "tid": tid
        },
        success:  function (response) {
          try {
            response = JSON.parse(response);
            if ( response.success ) {
              chrome.storage.local.set({ tid: false, data: data });
              if ( response.action == "accept" ) {
                View.inlineAlert("success", response.result);
              } else if (response.action == "wait") {
                View.inlineAlert("success", "waiting");
              } else {
                View.inlineAlert("cross", response.result);
              }
              $("#offerPanel").slideUp();
            } else {
              View.inlineAlert("error", response.error);
              if ( (response.error == "Attempting mobile confirmation: success"
                || response.error == "This offer is still pending. Please accept the trade offer and try again."
                || response.error == "Please accept the tradeoffer and wait. Checking tradeoffer...")
                && !this.app.steam.isOpened() ) {
                let tradelink = $("#offerContent").find("a").attr("href");
                if ( tradelink ) {
                  this.app.setSteamLinkOpend(true);
                  this.app.steam.setSteamLink(tradelink);
                  this.app.setSteamLinkOpend(true);
                  window.open(tradelink, "_blank");
                }
              }
            }
          } catch ( err ) {
            View.inlineAlert("error", "Javascript error: " + err);
          }

          if ( callback !== undefined ) {
              callback(response, data);
          }
        }.bind(this),
        error:    function (err) {
          View.inlineAlert("error", "AJAX error: " + err.statusText);
          if ( callback !== undefined ) {
            callback(
              {
                success: false,
                error:   "AJAX error: " + err.statusText,
                count:   0
              }, data
            );
          }
        },
        complete: function () {
          $this.prop("disabled", false);
        }
      }
    );
    return false;
  };

  /**
   * Attempt to confirm
   *
   * @param {{ success:boolean, error:string, count:Number }} response
   * @param {{ items:string, sum:Number }} data
   */
  tryConfirm (response, data) {
    if (!this.app.steam.isOpened()) {
      if ( response.success ) {
        if (response.action == "wait") {
          setTimeout(
            function () {
              if ( !this.app.stop ) {
                this.confirm(this.tryConfirm.bind(this), data);
              }
            }.bind(this), 20 * 1000
          );
        } else if (response.result == 'Exchange rejected.') {
          this.app.refresh();
          this.offering(data);
        } else {
          this.app.stopExtension();
        }
      } else if ( this.checkError(response.error) ) {
        this.app.stopExtension();
      } else {
        var count = response.count || 1;
        setTimeout(
          function () {
            if ( !this.app.stop ) {
              this.confirm(this.tryConfirm.bind(this), data);
            }
          }.bind(this), count * 1000
        );
      }
    } else {
      setTimeout(
        function () {
          if ( !this.app.stop ) {
            this.tryConfirm(response, data);
          }
        }.bind(this), 1000
      );
    }
  }

  /**
   * Confirming offer
   *
   * @param {{ items:string, sum:Number }} data
   */
  confirming (data) {
    this.confirm(this.tryConfirm.bind(this), data);
  }

  /**
   * Attempt to offer
   *
   * @param {{ success:boolean, error:string, count:Number }} response
   * @param {{ items:string, sum:Number }} data
   */
  tryOffer (response, data) {
    if ( response.success || response.error == 'You already have pending exchange with our bot. Please finish that one before making a new.' ) {
      this.confirming(data);
    } else if ( this.checkError(response.error) ) {
      this.app.stopExtension();
    } else {
      var count = response.count || 1;
      setTimeout(
        function () {
          if ( !this.app.stop ) {
            this.offer(this.tryOffer.bind(this), data);
          }
        }.bind(this), count * 1000
      );
    }
  }

  /**
   * Offering
   *
   * @param {{ items:string, sum:Number }} data
   */
  offering (data) {
    this.offer(this.tryOffer.bind(this), data);
  }
}