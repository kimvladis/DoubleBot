import App from './App';
import Double from './Double';
import io from 'socket.io-client';
import '../tools/unque';

export default class Socket {
  constructor () {
    this.app    = new App();
    this.socket = null;
    this.setUpSocket();
  }

  onHold (data) {
    console.log(data);
    this.app.heldItems = this.app.heldItems.concat(data.ids).unique();
  }

  onWithdraw (data) {
    this.app.isReady   = false;
    this.app.stop      = false;
    this.app.isOnGoing = true;
    this.app.double.offering(data);
  }

  /**
   * @param {{ host:string }} result
   */
  handler (result) {
    if ( this.app.host != result.host ) {
      this.app.host = result.host;

      if ( this.socket ) {
        io.destroy(this.socket);
      }

      this.socket = io(result.host);
      this.socket.on('held', this.onHold.bind(this));
      this.socket.on('withdraw', this.onWithdraw.bind(this));
      this.socket.on('connect', function () {
        this.app.status.notify();
      }.bind(this));
      this.socket.on('reconnect', function () {
        this.app.status.notify();
      }.bind(this));
    }
  }

  setUpSocket () {
    chrome.storage.local.get({ host: this.app.host }, this.handler.bind(this));
  }

  emit (event, message) {
    this.socket.emit(event, message);
  }
}