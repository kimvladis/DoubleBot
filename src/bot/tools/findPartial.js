Array.prototype.findPartial = function (value) {
  if (value) {
    return this.find(function (T, number, obj) {
      return value.indexOf(T) >= 0;
    });
  }

  return false;
};

export default Array.prototype.findPartial;