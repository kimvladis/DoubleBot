import App from './components/App';
import Steam from './components/Steam';

let app = new App();

$(function() {
  var interval = setInterval(
    function () {
      app.isItemsLoaded = $('#left_number').text() != 0;
      if ( app.isItemsLoaded ) {
        clearInterval(interval);
        app.afterLoad();
      }
    }, 10
  );
});

let steam = new Steam();
steam.acceptSteamTrade();