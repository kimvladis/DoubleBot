var webpack = require('webpack');
var path = require("path");
var CopyWebpackPlugin = require('copy-webpack-plugin');
var Crx = require("crx-webpack-plugin");
var fs = require('fs');
var manifest = JSON.parse(fs.readFileSync('./src/manifest.json', 'utf8'));

module.exports = {
  entry: {
    bot: "./src/bot/main.js",
    options: "./src/options/main.js",
    popup: "./src/popup/main.js",
    background: "./src/background/main.js"
  },
  output: {
    path: path.join(__dirname, "dist"),
    filename: "[name].min.js"
  },
  debug: true,
  devtool: 'source-map',
  plugins: [
    {
      apply: function(compiler) {
        compiler.plugin('done', function() {
          manifest = JSON.parse(fs.readFileSync('./src/manifest.json', 'utf8'));
        });
      }
    },
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
    new CopyWebpackPlugin(
      [
        { from: "src/manifest.json" },
        { from: "src/icon.png" },
        { from: "src/popup/popup.html" },
        { from: "src/options/options.html" }
      ],
      { copyUnmodified: true }
    ),
    new Crx({
      keyFile: 'csgodouble.pem',
      contentPath: 'dist',
      outputPath: 'ext',
      name: function () {
        return 'double_' + manifest.version;
      }
    })
  ],
  module:  {
    loaders: [
      {
        test:    /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader:  'babel'
      }, {
        test: /\.css$/,
        loaders: ['style', 'css']
      }, {
        test: /\.scss$/,
        loaders: ['style', 'css', 'postcss', 'sass']
      }, {
        test: /\.less$/,
        loaders: ['style', 'css', 'less']
      }, {
        test: /\.woff$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff&name=[path][name].[ext]"
      }, {
        test: /\.woff2$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff2&name=[path][name].[ext]"
      }, {
        test: /\.(eot|ttf|svg|gif|png)$/,
        loader: "file-loader"
      }
    ]
  }
};